﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechGig2017;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ExpertLevel()
        {
            for (int i = 0; i < 8; i++)
            {
                var x = GetMaxValue(i);
                Assert.AreEqual(x, Program.LaunchingSatellites(0, 0, GetInputArrayForTest(i)));
            }
        }

        private int GetMaxValue(int testDataId)
        {
            int max;
            switch (testDataId)
            {
                case 1:
                    max = 4;
                    break;
                case 2:
                    max = 4;
                    break;
                case 3:
                    max = 3;
                    break;
                case 4:
                    max = 0;
                    break;
                case 5:
                    max = 3;
                    break;
                case 6:
                    max = 3;
                    break;
                case 7:
                    max = 3;
                    break;
                default:
                    max = 3;
                    break;

            }
            return max;
        }

        private string[] GetInputArrayForTest(int testDataId)
        {
            string[] array;
            switch (testDataId)
            {
                case 1:
                    array = new[]
                    {
                        "1#1", "2#2","4#2", "3#3", "2#4", "1#5", "3#5", "4#5"
                    };
                    break;
                case 2:
                    array = new[]
                    {
                        "1#1", "1#3","2#4", "4#1", "4#2", "1#4", "1#6", "4#3", "4#4", "4#6", "3#4"
                    };
                    break;
                case 3:
                    array = new[]
                    {
                        "1#1", "2#3", "3#4", "1#5", "3#1", "5#1", "5#3", "5#4"
                    };
                    break;
                case 4:
                    array = new[]
                    {
                        "1#2", "2#2", "3#1", "3#4", "4#4", "5#2"
                    };
                    break;
                case 5:
                    array = new[]
                    {
                        "1#1", "2#1", "4#1", "3#4", "4#3", "4#4", "4#7", "1#5", "3#5"
                    };
                    break;
                case 6:
                    array = new[]
                    {
                        "1#1", "2#1", "4#1", "3#4", "4#3", "4#4", "4#7", "1#5", "3#5"
                    };
                    break;
                case 7:
                    array = new[]
                    {
                        "1#0", "2#1", "3#2", "4#4", "8#8"
                    };
                    break;
                default:
                    array = new[]
                    {
                        "2#1", "2#5", "2#6", "2#7", "2#3","6#1", "6#2","6#3","6#5", "6#7", "6#4"
                    };
                    break;
            }
            return array;
        }
    }
}
