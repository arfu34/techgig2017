﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
// ReSharper disable CompareOfFloatsByEqualityOperator
// ReSharper disable InconsistentNaming

namespace TechGig2017
{
    public class Program
    {
        static void Main(string[] args)
        {
            //EasyLevel();
            //ExpertLevel();
            //MediumLevel();
            //Semifinal1();
            Semifinal2();
            Console.ReadLine();
        }

        #region Semi-final P2

        private static void Semifinal2()
        {
            //var minPerimeter = FindMinPerimeter(6, 5, 7, 3, 7, 2, new [,] {{3, 4}, {3, 3}, {6, 1}, {1, 1}, {5, 5}, {5, 5}, {3, 1}});
            var minPerimeter = FindMinPerimeter(250, 250, 5000, 2500, 5000, 2, GetInputHubs(5000));
        }
        private static readonly List<Rectangle> totalRect = new List<Rectangle>();
        private static int FindMinPerimeter(int input1, int input2, int input3, int input4, int input5_size_rows, int input5_size_cols, int[,] input5)
        {
            int perimeter = 0;
            List<CoOrdinate> totalHubs = new List<CoOrdinate>();
            for (int i = 0; i < input5.GetLength(0); i++)
            {
                CoOrdinate sCoOrdinate = new CoOrdinate() {XCoOrdinate = input5[i, 1], YCoOrdinate = input5[i, 0] };
                totalHubs.Add(sCoOrdinate);
            }
            totalHubs = totalHubs.OrderBy(x => x.XCoOrdinate).ThenBy(x => x.YCoOrdinate).ToList();
            var duplicates = (from hub in totalHubs group hub by new {hub.YCoOrdinate, hub.XCoOrdinate}).Where(x=>x.Count() > 1);

            
            List<CoOrdinate> tempCoOrdinates = new List<CoOrdinate>();

            for (int i = 0; i < totalHubs.Count; i++)
            {
                var hub = totalHubs[i];
                Rectangle rect = new Rectangle()
                {
                    LTop = new CoOrdinate() { XCoOrdinate = hub.XCoOrdinate, YCoOrdinate = hub.YCoOrdinate },
                    LBottom = new CoOrdinate() { XCoOrdinate = hub.XCoOrdinate, YCoOrdinate = hub.YCoOrdinate },
                    RTop = new CoOrdinate() { XCoOrdinate = hub.XCoOrdinate, YCoOrdinate = hub.YCoOrdinate },
                    RBottom = new CoOrdinate() { XCoOrdinate = hub.XCoOrdinate, YCoOrdinate = hub.YCoOrdinate }
                };
                IEnumerable<CoOrdinate> lstCo = null;
                if (duplicates.Any(x=> x.Count() == input4 && x.Key.XCoOrdinate == hub.XCoOrdinate && x.Key.YCoOrdinate == hub.YCoOrdinate))
                {
                    //rect.PointsInRect = lstCo;
                    totalRect.Add(rect);
                    continue;
                }

                var rectHorizontal = ExpandHorizontally(totalHubs, input4, rect.Clone(), input2, input1);
                if (rectHorizontal != null) totalRect.Add(rectHorizontal);
                var rectVertical = ExpandVertically(totalHubs, input4, rect.Clone(), input2, input1);
                if (rectVertical != null) totalRect.Add(rectVertical);
                var rectDiaonal = ExpandDiagonal(totalHubs, input4, rect.Clone(), input2, input1, rectVertical != null, rectHorizontal != null);
                if (rectDiaonal != null && rectDiaonal.Count > 0) totalRect.AddRange(rectDiaonal);
            }

            List<Tuple<Rectangle, Rectangle>> nonIntersectingTuple = new List<Tuple<Rectangle, Rectangle>>();
            for(int i = 0; i < totalRect.Count; i++)
            {
                var rect = totalRect[i];
                for (int j = i; j< totalRect.Count; j++)
                {
                    var rectJ = totalRect[j];
                    if(RectNonIntersect(rect, rectJ))
                    {
                        nonIntersectingTuple.Add(new Tuple<Rectangle, Rectangle>(rect, rectJ));
                    }
                }
            }

            var per = Int32.MaxValue;
            foreach (var tuple in nonIntersectingTuple)
            {
                var temp = ((tuple.Item1.Width + tuple.Item1.Height) * 2) +
                           ((tuple.Item2.Width + tuple.Item2.Height) * 2);
                if (temp < per) per = temp;
            }
            if (per != Int32.MaxValue) perimeter = per;
            return perimeter;
        }

        private static Rectangle ExpandHorizontally(List<CoOrdinate> hubList, int kHubs, Rectangle rect, int rows, int cols)
        {
            Rectangle rectReturn = null;
            while (true)
            {
                var rtop = rect.RTop;
                var rbot = rect.RBottom;
                rtop.YCoOrdinate = rtop.YCoOrdinate + 1;
                rbot.YCoOrdinate = rbot.YCoOrdinate + 1;
                rect.RTop = rtop;
                rect.RBottom = rbot;
                if (rtop.YCoOrdinate > cols || rbot.YCoOrdinate > cols) break;
                IEnumerable<CoOrdinate> lstCoHorizontal = null;
                if(ArePointsInRect(rect, hubList, kHubs, out lstCoHorizontal))
                {
                    rectReturn = rect;
                    rectReturn.PointsInRect = lstCoHorizontal;
                    break;
                }
            }
            return rectReturn;
        }

        private static Rectangle ExpandVertically(List<CoOrdinate> hubList, int kHubs, Rectangle rect, int rows, int cols)
        {
            Rectangle rectReturn = null;
            IEnumerable<CoOrdinate> lstCoVertical = null;
            while (true)
            {
                var rbot = rect.RBottom;
                var lbot = rect.LBottom;
                lbot.XCoOrdinate = lbot.XCoOrdinate + 1;
                rbot.XCoOrdinate = rbot.XCoOrdinate + 1;
                rect.RBottom = rbot;
                rect.LBottom = lbot;
                if (lbot.XCoOrdinate > rows || rbot.XCoOrdinate > rows) break;
                if (ArePointsInRect(rect, hubList, kHubs, out lstCoVertical))
                {
                    rectReturn = rect;
                    rectReturn.PointsInRect = lstCoVertical;
                    break;
                }
            }
            return rectReturn;
        }

        private static List<Rectangle> ExpandDiagonal(List<CoOrdinate> hubList, int kHubs, Rectangle rect, int rows, int cols, bool verticaltraversed, bool horizontaltraversed)
        {
            List <Rectangle> rectReturn = new List<Rectangle>();
            IEnumerable<CoOrdinate> lstCoDiagonal = null;
            while (true)
            {
                var rbot = rect.RBottom;
                var lbot = rect.LBottom;
                var rtop = rect.RTop;
                lbot.XCoOrdinate = lbot.XCoOrdinate + 1;
                rbot.XCoOrdinate = rbot.XCoOrdinate + 1;
                rbot.YCoOrdinate = rbot.YCoOrdinate + 1;
                rtop.YCoOrdinate = rtop.YCoOrdinate + 1;
                rect.RBottom = rbot;
                rect.LBottom = lbot;
                rect.RTop = rtop;
                if (lbot.XCoOrdinate > rows || rbot.XCoOrdinate > rows || rtop.XCoOrdinate > rows || rtop.YCoOrdinate > cols) break;

                if (ArePointsInRect(rect, hubList, kHubs, out lstCoDiagonal))
                {
                    rect.PointsInRect = lstCoDiagonal;
                    rectReturn.Add(rect);                    
                    break;
                }

                if (!horizontaltraversed)
                {
                    var rectHorizontal = ExpandHorizontally(hubList, kHubs, rect.Clone(), rows, cols);
                    if (rectHorizontal != null)
                    {
                        rectReturn.Add(rectHorizontal);
                        horizontaltraversed = true;
                    }
                }

                if (!verticaltraversed)
                {
                    var rectVertical = ExpandVertically(hubList, kHubs, rect.Clone(), rows, cols);
                    if (rectVertical != null)
                    {
                        rectReturn.Add(rectVertical);
                        verticaltraversed = true;
                    }
                }

                if (verticaltraversed && horizontaltraversed)
                {
                    break;
                }
            }
            return rectReturn;
        }

        private static bool ArePointsInRect(Rectangle rect, List<CoOrdinate> hubList, int kHubs, out IEnumerable<CoOrdinate> lstPts)
        {
            lstPts = null;
            return hubList.Count(x => (x.XCoOrdinate >= rect.LTop.XCoOrdinate && x.XCoOrdinate <= rect.LBottom.XCoOrdinate) && (x.YCoOrdinate >= rect.LTop.YCoOrdinate && x.YCoOrdinate <= rect.RTop.YCoOrdinate)) == kHubs;
        }

        private static bool RectNonIntersect(Rectangle R1, Rectangle R2)
        {
            if ((R1.LTop.XCoOrdinate > R2.RBottom.XCoOrdinate) || (R1.RBottom.XCoOrdinate < R2.LTop.XCoOrdinate) || (R1.LTop.YCoOrdinate > R2.RBottom.YCoOrdinate) || (R1.RBottom.YCoOrdinate < R2.LTop.YCoOrdinate))
            {
                return true;
            }
            return false;
        }

        public class Rectangle
        {
            public IEnumerable<CoOrdinate> PointsInRect;
            public CoOrdinate RTop;
            public CoOrdinate RBottom;
            public CoOrdinate LTop;
            public CoOrdinate LBottom;
            public int Width
            {
                get { return (RBottom.YCoOrdinate - LBottom.YCoOrdinate) + 1; }
            }

            public int Height
            {
                get { return (RBottom.XCoOrdinate - RTop.XCoOrdinate) + 1; }
            }

            public Rectangle Clone()
            {
                return new Rectangle()
                {
                    RTop = this.RTop.Clone(),
                    LTop = this.LTop.Clone(),
                    LBottom = this.LBottom.Clone(),
                    RBottom = this.RBottom.Clone()
                };
            }
        }

        #endregion

        #region Semi-final P1

        private static void Semifinal1()
        {            
            var minSteps = Distance(2, 1, 3, 3);
            Console.WriteLine( $" count {minSteps}");
            Console.ReadLine();
        }

        private static int FindMinStepsToCheckMate(int sourceX, int sourceY, int destX, int destY)
        {
            var minSteps = 0;

            CoOrdinate source = new CoOrdinate() { XCoOrdinate = sourceX, YCoOrdinate = sourceY };
            CoOrdinate destination = new CoOrdinate() { XCoOrdinate = destX, YCoOrdinate = destY };

            if (source.XCoOrdinate != destination.XCoOrdinate || source.YCoOrdinate != destination.YCoOrdinate)
            {

                List<CoOrdinate> traversedPoints = new List<CoOrdinate>();
                Dictionary<CoOrdinate, int> possiblePoints = new Dictionary<CoOrdinate, int>();
                possiblePoints.Add(source, 0);
                while(possiblePoints.Count > 0)
                {
                    var dict = possiblePoints.Aggregate((l, r) => l.Value < r.Value ? l : r);
                    var coordinate = dict.Key;
                    traversedPoints.Add(coordinate);
                    var step = dict.Value;
                    // 1, 2
                    CoOrdinate probable1 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate + 1, YCoOrdinate = coordinate.YCoOrdinate + 2 };
                    if(probable1.XCoOrdinate == destination.XCoOrdinate && probable1.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable1.XCoOrdinate <= 8 && probable1.YCoOrdinate <= 8 && probable1.XCoOrdinate > 0 && probable1.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable1))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable1.XCoOrdinate && x.YCoOrdinate == probable1.YCoOrdinate))
                            {
                                possiblePoints.Add(probable1, step + 1);
                            }
                        }
                    }

                    // -1, 2
                    CoOrdinate probable2 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate - 1, YCoOrdinate = coordinate.YCoOrdinate + 2 };
                    if (probable2.XCoOrdinate == destination.XCoOrdinate && probable2.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable2.XCoOrdinate <= 8 && probable2.YCoOrdinate <= 8 && probable2.XCoOrdinate > 0 && probable2.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable2))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable2.XCoOrdinate && x.YCoOrdinate == probable2.YCoOrdinate))
                            {
                                possiblePoints.Add(probable2, step + 1);
                            }
                        }
                    }

                    // 1, -2
                    CoOrdinate probable3 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate + 1, YCoOrdinate = coordinate.YCoOrdinate - 2 };
                    if (probable3.XCoOrdinate == destination.XCoOrdinate && probable3.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable3.XCoOrdinate <= 8 && probable3.YCoOrdinate <= 8 && probable3.XCoOrdinate > 0 && probable3.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable3))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable3.XCoOrdinate && x.YCoOrdinate == probable3.YCoOrdinate))
                            {
                                possiblePoints.Add(probable3, step + 1);
                            }
                        }
                    }

                    // -1, -2
                    CoOrdinate probable4 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate - 1, YCoOrdinate = coordinate.YCoOrdinate - 2 };
                    if (probable4.XCoOrdinate == destination.XCoOrdinate && probable4.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable4.XCoOrdinate <= 8 && probable4.YCoOrdinate <= 8 && probable4.XCoOrdinate > 0 && probable4.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable4))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable4.XCoOrdinate && x.YCoOrdinate == probable4.YCoOrdinate))
                            {
                                possiblePoints.Add(probable4, step + 1);
                            }
                        }
                    }

                    // 2, 1
                    CoOrdinate probable5 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate + 2, YCoOrdinate = coordinate.YCoOrdinate + 1 };
                    if (probable5.XCoOrdinate == destination.XCoOrdinate && probable5.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable5.XCoOrdinate <= 8 && probable5.YCoOrdinate <= 8 && probable5.XCoOrdinate > 0 && probable5.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable5))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable5.XCoOrdinate && x.YCoOrdinate == probable5.YCoOrdinate))
                            {
                                possiblePoints.Add(probable5, step + 1);
                            }
                        }
                    }

                    // -2, 1
                    CoOrdinate probable6 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate - 2, YCoOrdinate = coordinate.YCoOrdinate + 1 };
                    if (probable6.XCoOrdinate == destination.XCoOrdinate && probable6.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable6.XCoOrdinate <= 8 && probable6.YCoOrdinate <= 8 && probable6.XCoOrdinate > 0 && probable6.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable6))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable6.XCoOrdinate && x.YCoOrdinate == probable6.YCoOrdinate))
                            {
                                possiblePoints.Add(probable6, step + 1);
                            }
                        }
                    }

                    // 2, -1
                    CoOrdinate probable7 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate + 2, YCoOrdinate = coordinate.YCoOrdinate - 1 };
                    if (probable7.XCoOrdinate == destination.XCoOrdinate && probable7.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable7.XCoOrdinate <= 8 && probable7.YCoOrdinate <= 8 && probable7.XCoOrdinate > 0 && probable7.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable7))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable7.XCoOrdinate && x.YCoOrdinate == probable7.YCoOrdinate))
                            {
                                possiblePoints.Add(probable7, step + 1);
                            }
                        }
                    }

                    // -2, -1
                    CoOrdinate probable8 = new CoOrdinate() { XCoOrdinate = coordinate.XCoOrdinate - 2, YCoOrdinate = coordinate.YCoOrdinate - 1 };
                    if (probable8.XCoOrdinate == destination.XCoOrdinate && probable8.YCoOrdinate == destination.YCoOrdinate)
                    {
                        minSteps = step + 1;
                        break;
                    }
                    else if (probable8.XCoOrdinate <= 8 && probable8.YCoOrdinate <= 8 && probable8.XCoOrdinate > 0 && probable8.YCoOrdinate > 0)
                    {
                        if (!possiblePoints.ContainsKey(probable8))
                        {
                            if (!traversedPoints.Any(x => x.XCoOrdinate == probable8.XCoOrdinate && x.YCoOrdinate == probable8.YCoOrdinate))
                            {
                                possiblePoints.Add(probable8, step + 1);
                            }
                        }
                    }
                    possiblePoints.Remove(coordinate);
                }
            }

            return minSteps;
        }

        public struct CoOrdinate
        {
            public int XCoOrdinate;
            public int YCoOrdinate;
            public CoOrdinate Clone()
            {
                return new CoOrdinate() { XCoOrdinate = this.XCoOrdinate, YCoOrdinate = this.YCoOrdinate };
            }
        }

        private static bool Test(int x1, int y1, int x2, int y2, int sourcex, int sourcey, int destx, int desty)
        {
            return (sourcex == x1 && sourcey == y1 && destx == x2 && desty == y2) || (sourcex == x2 && sourcey == y2 && destx == x1 && desty == y1);
        }        

        private static int Distance(int sx, int sy, int tx, int ty)
        {            
            if (Test(1, 1, 2, 2, sx, sy, tx, ty) ||
                Test(7, 7, 8, 8, sx, sy, tx, ty) ||
                Test(7, 2, 8, 1, sx, sy, tx, ty) ||
                Test(1, 8, 2, 7, sx, sy, tx, ty))
            {
                return 4;
            }
            
            int x = Math.Abs(sx - tx);
            int y = Math.Abs(sy - ty);
            
            if (x < y)
            {
                int t = x;
                x = y;
                y = t;
            }
            
            if (x == 1 && y == 0)
                return 3;
            if (x == 2 && y == 2)
                return 4;
            
            double delta = x - y;
            if (y > delta)
            {
                return (int)(delta - 2 * Math.Floor((delta - y) / 3));
            }
            else
            {
                return (int)(delta - 2 * Math.Floor((delta - y) / 4));
            }
        }
        #endregion

        #region Easy

        private static void EasyLevel()
        {
            string pattern = "ascba";//RandomString(5000);
            var minNumber = OpenDoorForMe(pattern, 5);
        }

        private static int OpenDoorForMe(string input2, int input1)
        {
            var reverse = input2.ToCharArray();
            Array.Reverse(reverse);
            return input1 - LongestPalindrome(input2.ToCharArray(), reverse, input1);
        }

        private static int LongestPalindrome(char[] str1, char[] str2, int size)
        {
            int[,] l = new int[size + 1, size + 1];

            for (int i = 1; i <= str1.Length; i++)
            {
                for (int j = 1; j <= str2.Length; j++)
                {
                    if (str1[i - 1] == str2[j - 1])
                    {
                        l[i, j] = l[i - 1, j - 1] + 1;
                    }
                    else
                    {
                        l[i, j] = Math.Max(l[i - 1, j], l[i, j - 1]);
                    }
                }
            }
            return l[str1.Length, str2.Length];
        }

        #endregion

        #region Expert
        
        private static void ExpertLevel()
        {
            int input1 = 0;
            int input2 = 6000;
            //string[] input3 = new[]
            //{
            //    "2#1", "4#2", "2#5", "2#6", "2#7", "2#3", "3#4", "6#1", "6#5", "6#7", "6#4"
            //};
            string[] input3 = new[]
            {
                "4#1", "4#3", "3#3", "2#5", "1#3"
            };
            //for (int i = 0; i < 50; i++)
            //{
            //string[] input3 = GetInputArray(6);
            var maxGoodPath = LaunchingSatellites(input1, input2, input3);
            //}
        }

        public static int LaunchingSatellites(int input1, int input2, string[] input3)
        {
            int maxCount = 0;
            int noOfSatellites = input3.Length;
            //input3 = input3.Where(x => !x.Equals(noOfSatellites.ToString())).ToArray();
            Array.Sort(input3);
            Node[] satelliteMatrix = new Node[noOfSatellites];
            for (int i = 0; i < noOfSatellites; i++)
            {
                string[] coordinates = input3[i].Split('#');
                int r = Convert.ToInt32(coordinates[0]);
                int c = Convert.ToInt32(coordinates[1]);
                satelliteMatrix[i] = new Node(r, c);
            }
            maxCount = SearchMax(satelliteMatrix, noOfSatellites);
            return maxCount;
        }

        #region ExpertSlope

        private static int SearchMax(Node[] satelliteMatrix, int noOfSatellites)
        {
            List<Point> collinearPaths = new List<Point>();
            List<ListNode> dict = new List<ListNode>();
            double res = 0;
            for (int j = 0; j < noOfSatellites; j++)
            {
                var nod = satelliteMatrix[j];
                int rowOfInSearchSatellite = nod.XPoint;
                int colOfInSearchSatellite = nod.YPoint;
                for (int i = j+1; i < noOfSatellites; i++)
                {
                    var anotherNode = satelliteMatrix[i];
                    if((rowOfInSearchSatellite == anotherNode.XPoint && colOfInSearchSatellite == anotherNode.YPoint)) continue;

                    if (rowOfInSearchSatellite == anotherNode.XPoint && colOfInSearchSatellite != anotherNode.YPoint)
                    {
                        var ct = dict.FirstOrDefault(x => x.Slope == null && x.IsHorizontal);
                        if (ct != null)
                        {
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            ct.CollinearPoint.Add(x);
                        }
                        else
                        {
                            var p = new Point(nod.XPoint, nod.YPoint, null);
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            p.Add(x);
                            dict.Add(new ListNode(){ CollinearPoint = p, Slope = null, IsHorizontal = true, IsVertical = false});

                        }
                    }
                    else if (rowOfInSearchSatellite != anotherNode.XPoint && colOfInSearchSatellite == anotherNode.YPoint)
                    {
                        var ct = dict.FirstOrDefault(x => x.Slope == null && x.IsVertical);
                        if (ct != null)
                        {
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            ct.CollinearPoint.Add(x);
                        }
                        else
                        {
                            var p = new Point(nod.XPoint, nod.YPoint, null);
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            p.Add(x);
                            dict.Add(new ListNode() { CollinearPoint = p, Slope = null, IsHorizontal = false, IsVertical = true });
                        }
                    }
                    else
                    {
                        var slope = (anotherNode.YPoint - colOfInSearchSatellite) / ((double)(anotherNode.XPoint - rowOfInSearchSatellite));
                        var ct = dict.FirstOrDefault(x => x.Slope == slope && !x.IsHorizontal && !x.IsVertical);
                        if (ct != null)
                        {
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            ct.CollinearPoint.Add(x);
                        }
                        else
                        {
                            var p = new Point(nod.XPoint, nod.YPoint, null);
                            var x = new Point(anotherNode.XPoint, anotherNode.YPoint, null);
                            p.Add(x);
                            dict.Add(new ListNode() { CollinearPoint = p, Slope = slope, IsHorizontal = false, IsVertical = false });
                        }
                    }
                }
                foreach (var d in dict)
                {
                    if(d.CollinearPoint.Count < 3) continue;
                    collinearPaths.Add(d.CollinearPoint);
                    if (d.CollinearPoint.Distance != null)
                    {
                        res = Math.Max(res, d.CollinearPoint.Distance.Value);
                    }
                }
                dict.Clear();
            }
            var maxResult = collinearPaths.Where(x => x.IsEquiDistance);
            int maxCount = 0;
            if (maxResult.Any())
            {
                maxCount = maxResult.Max(x => x.Count);
            }
            return maxCount;
        }

        private class Point
        {
            public bool CheckEquidistance(Point p)
            {
                bool isEqual = false;
                double? dist = this.Distance;
                var point = this;
                while (point.NextPoint != null)
                {
                    point = point.NextPoint;
                }
                if (DistanceBetweenCoordinates(point.X, point.Y, p.X, p.Y) == dist || dist == null)
                {
                    isEqual = true;
                }
                return isEqual;
            }

            public void Add(Point p)
            {
                var point = this;
                while (point.NextPoint != null)
                {
                    point = point.NextPoint;
                }
                point.NextPoint = p;
            }

            public bool IsEquiDistance
            {
                get
                {
                    bool isEqual = true;
                    var point = this;
                    while (point.NextPoint != null)
                    {
                        var dist = point.Distance;
                        isEqual = ((this.NextPoint.Distance == dist || this.NextPoint.Distance == null)) || dist == null;
                        if (!isEqual) break;
                        point = point.NextPoint;
                    }
                    return isEqual;
                }
            }

            public int Count
            {
                get
                {
                    int x = 1;
                    var point = this;
                    while (point.NextPoint != null)
                    {
                        point = point.NextPoint;
                        x++;
                    }
                    return x;
                }
            }

            private int X;
            private int Y;

            public double? Distance
            {
                get
                {
                    double? distance = null;
                    if (NextPoint != null)
                    {
                        distance = DistanceBetweenCoordinates(X, Y, NextPoint.X, NextPoint.Y);
                    }
                    return distance;
                }
            }
            private Point NextPoint { get; set; }

            public Point Clone(Point p)
            {
                if (p == null) return null;
                var pt = new Point(p.X, p.Y, p.Clone(p.NextPoint));
                return pt;
            }

            public Point(int x, int y, Point point)
            {
                X = x;
                Y = y;
                NextPoint = point;
            }
            private static double DistanceBetweenCoordinates(int x1, int y1, int x2, int y2)
            {
                return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            }
        }

        private class ListNode
        {
            public double? Slope { get; set; }
            public bool IsVertical { get; set; }
            public bool IsHorizontal { get; set; }
            public Point CollinearPoint { get; set; }
        }

        private class Node
        {
            public int XPoint { get; set; }
            public int YPoint { get; set; }

            public Node(int xPoint, int yPoint)
            {
                XPoint = xPoint;
                YPoint = yPoint;
            }
        }
        #endregion

        #endregion

        #region Medium

        private static void MediumLevel()
        {
            int input1 = 40; //total no of cards available
            int input2 = 4; //no of chances available
            string input3 = "16"; // , comma separated string which are turned towards white side in the final combination
            string input4 = "2,25";
            //int input1 = 100; //total no of cards available
            //int input2 = 10000; //no of chances available
            //string input3 = "-1"; // , comma separated string which are turned towards white side in the final combination
            //string input4 = "-1";
            //int input1 = 10; //total no of cards available
            //int input2 = 1; //no of chances available
            //string input3 = "8"; // , comma separated string which are turned towards white side in the final combination
            //string input4 = "2,7";
            CombinationCards(input1, input2, input3, input4);
        }
        static List<string> opList = new List<string>();
        static List<Combination> traversedList = new List<Combination>();
        static List<Combination> temptraversedList = new List<Combination>();
        public static int Count
        {
            get { return traversedList.Count; }  
        } 

        private static string CombinationCards(int input1, int input2, string input3, string input4)
        {
            string retValue = "";
            string initialCombination = "";
            for (int i = 0; i < input1; i++)
            {
                initialCombination += '1';
            }
            traversedList.Add(new Combination(initialCombination, 0, false));
            //opList.Add(initialCombination);
            int ix = 0;
            while (ix < Count)
            {
                var firstOrDefault = traversedList[ix];
                if (firstOrDefault != null)
                {
                    TryCombinations(input1, input2, input3, input4, firstOrDefault.CombinationStr, firstOrDefault.Index);
                    firstOrDefault.IsTraversed = true;
                }
                ix++;
            }
            opList.Sort();
            foreach (var str in opList)
            {
                if (string.IsNullOrEmpty(retValue))
                {
                    retValue += str;
                }
                else
                {
                    retValue += "#" + str;
                }
            }
            return retValue;
        }

        internal class Combination
        {
            public string CombinationStr { get; set; }
            public int Index { get; set; }
            public bool IsTraversed { get; set; }

            public Combination(string combinationStr, int index, bool isTraversed)
            {
                CombinationStr = combinationStr;
                Index = index;
                IsTraversed = isTraversed;
            }
        }

        private static void TryCombinations(int input1, int input2, string input3, string input4, string initialCombination, int i)
        {
            temptraversedList.Add(new Combination(initialCombination, i, true));
            string s = null;
            s = ActionFlipAll(input1, input2, input3, input4, initialCombination, i == input2 - 1);
            if (!string.IsNullOrEmpty(s))
            {
                if (i == input2 - 1)
                {
                    if (opList.All(x => !x.Equals(s))) opList.Add(s);
                }
                else
                {
                    if (!temptraversedList.Any(x => x.CombinationStr.Equals(s)))
                    {
                        traversedList.Add(new Combination(s, i + 1, false));
                    }
                }
            }
            s = ActionFlipOdd(input1, input2, input3, input4, initialCombination, i == input2 - 1);
            if (!string.IsNullOrEmpty(s))
            {
                if (i == input2 - 1)
                {
                    if (opList.All(x => !x.Equals(s)))
                        opList.Add(s);
                }
                else
                {
                    if (!temptraversedList.Any(x => x.CombinationStr.Equals(s)))
                    {
                        traversedList.Add(new Combination(s, i + 1, false));
                    }
                }
            }
            s = ActionFlipEven(input1, input2, input3, input4, initialCombination, i == input2 - 1);
            if (!string.IsNullOrEmpty(s))
            {
                if (i == input2 - 1)
                {
                    if (opList.All(x => !x.Equals(s))) opList.Add(s);
                }
                else
                {
                    if (!temptraversedList.Any(x => x.CombinationStr.Equals(s)))
                    {
                        traversedList.Add(new Combination(s, i + 1, false));
                    }
                }
            }
            s = ActionFlip3KPlus1(input1, input2, input3, input4, initialCombination, i == input2 - 1);
            if (!string.IsNullOrEmpty(s))
            {
                if (i == input2 - 1)
                {
                    if (opList.All(x => !x.Equals(s))) opList.Add(s);
                }
                else
                {
                    if (!temptraversedList.Any(x => x.CombinationStr.Equals(s)))
                    {
                        traversedList.Add(new Combination(s, i + 1, false));
                    }
                }
            }
        }

        private static string ActionFlipAll(int input1, int input2, string input3, string input4, string combination, bool checkRestrictions)
        {
            string returnString = null;
            bool isPossible = true;
            if (checkRestrictions && input4.Length > 0 && !input4.Contains("-1"))
            {
                var blackPositions = input4.Split(',');
                foreach (var ch in blackPositions)
                {
                    if (combination[int.Parse(ch) - 1] == '0')
                    {
                        isPossible = false;
                        break;
                    }
                }
            }
            if (checkRestrictions && input3.Length > 0 && isPossible && !input3.Contains("-1"))
            {
                var whitePositions = input3.Split(',');
                foreach (var ch in whitePositions)
                {
                    if (combination[int.Parse(ch) - 1] == '1')
                    {
                        isPossible = false;
                        break;
                    }
                }
            }
            if (isPossible)
            {
                foreach (var c in combination)
                {
                    if (c == '0')
                    {
                        returnString += "1";
                    }
                    else
                    {
                        returnString += "0";
                    }
                }
            }

            return returnString;
        }

        private static string ActionFlipOdd(int input1, int input2, string input3, string input4, string combination, bool checkRestrictions)
        {
            string returnString = null;
            bool isPossible = true;

            if (checkRestrictions && input4.Length > 0 && !input4.Contains("-1"))
            {
                var blackPositions = input4.Split(',');
                foreach (var ch in blackPositions)
                {
                    if (int.Parse(ch) % 2 != 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }
            if (checkRestrictions && input3.Length > 0 && isPossible && !input3.Contains("-1"))
            {
                var whitePositions = input3.Split(',');
                foreach (var ch in whitePositions)
                {
                    if (int.Parse(ch) % 2 != 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }

            if (isPossible)
            {
                for (int i = 0; i < input1; i++)
                {
                    if ((i + 1) % 2 == 0)
                    {
                        returnString += combination[i];
                    }
                    else
                    {
                        if (combination[i] == '0')
                        {
                            returnString += "1";
                        }
                        else
                        {
                            returnString += "0";
                        }
                    }
                }
            }

            return returnString;
        }

        private static string ActionFlipEven(int input1, int input2, string input3, string input4, string combination, bool checkRestrictions)
        {
            string returnString = null;
            bool isPossible = true;
            if (checkRestrictions && input4.Length > 0 && !input4.Contains("-1"))
            {
                var blackPositions = input4.Split(',');
                foreach (var ch in blackPositions)
                {
                    if (int.Parse(ch) % 2 == 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }
            if (checkRestrictions && input3.Length > 0 && isPossible && !input3.Contains("-1"))
            {
                var whitePositions = input3.Split(',');
                foreach (var ch in whitePositions)
                {
                    if (int.Parse(ch) % 2 == 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }

            if (isPossible)
            {
                for (int i = 0; i < input1; i++)
                {
                    if ((i + 1) % 2 != 0)
                    {
                        returnString += combination[i];
                    }
                    else
                    {
                        if (combination[i] == '0')
                        {
                            returnString += "1";
                        }
                        else
                        {
                            returnString += "0";
                        }
                    }
                }
            }

            return returnString;
        }

        private static string ActionFlip3KPlus1(int input1, int input2, string input3, string input4, string combination, bool checkRestrictions)
        {
            string returnString = null;
            bool isPossible = true;
            if (checkRestrictions && input4.Length > 0 && !input4.Contains("-1"))
            {
                var blackPositions = input4.Split(',');
                foreach (var ch in blackPositions)
                {
                    if ((int.Parse(ch) -1) % 3 == 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }
            if (checkRestrictions && input3.Length > 0 && isPossible && !input3.Contains("-1"))
            {
                var whitePositions = input3.Split(',');
                foreach (var ch in whitePositions)
                {
                    if ((int.Parse(ch) - 1) % 3 == 0)
                    {
                        if (combination[int.Parse(ch) - 1] == '1')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                    else
                    {
                        if (combination[int.Parse(ch) - 1] == '0')
                        {
                            isPossible = false;
                            break;
                        }
                    }
                }
            }

            if (isPossible)
            {
                for (int i = 0; i < input1; i++)
                {
                    if (i % 3 != 0)
                    {
                        returnString += combination[i];
                    }
                    else
                    {
                        if (combination[i] == '0')
                        {
                            returnString += "1";
                        }
                        else
                        {
                            returnString += "0";
                        }
                    }
                }
            }

            return returnString;
        }
        #endregion

        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string[] GetInputArray(int size)
        {
            string[] arr = new string[size];
            for (int i = 0; i < arr.Length; i++)
            {
                var str = random.Next(1, 6) + "#" + random.Next(1, 5);
                while (arr.Any(x=>x!=null && x.Equals(str)))
                {
                    str = random.Next(1, 6) + "#" + random.Next(1, 5);
                }
                arr[i] = str;
            }
            return arr;
        }
        private static int[,] GetInputHubs(int size)
        {
            int[,] arr = new int[size, 2];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                arr[i, 0] = random.Next(1, 50);
                arr[i, 1] = random.Next(1, 50);
            }
            return arr;
        }
    }
    
}
